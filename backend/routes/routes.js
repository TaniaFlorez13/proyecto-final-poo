// 
const pool = require("../data/config");

// Route the app
const router = (app) => {
  // 
  app.get("/", (request, response) => {
    response.send({
      message: "Welcome to the Node.js Express REST API!",
    });
  });

  // Trae todas las categorias
  app.get("/categories", (request, response) => {
    pool.query("SELECT * FROM categoria", (error, result) => {
      if (error) throw error;

      response.send(result);
    });
  });

  // 
  app.get("/category/:id", (request, response) => {
    const id = request.params.id;

    pool.query("SELECT * FROM categoria WHERE id = ?", id, (error, result) => {
      if (error) throw error;

      response.send(result);
    });
  });

  // y nuevo usuario
  app.post("/sites", (request, response) => {
    pool.query("INSERT INTO sitio SET ?", request.body, (error, result) => {
      if (error) throw error;

      response.status(201).send(`User added with ID: ${result.insertId}`);
    });
  });

  // Update an existing user
  app.put("/users/:id", (request, response) => {
    const id = request.params.id;

    pool.query(
      "UPDATE users SET ? WHERE id = ?",
      [request.body, id],
      (error, result) => {
        if (error) throw error;

        response.send("User updated successfully.");
      }
    );
  });

  // Delete a user
  app.delete("/users/:id", (request, response) => {
    const id = request.params.id;

    pool.query("DELETE FROM users WHERE id = ?", id, (error, result) => {
      if (error) throw error;
      response.send("User deleted.");
    });
  });

  // Trae todos los sitios
  app.get("/sites", (request, response) => {
    pool.query("SELECT * FROM sitio", (error, result) => {
      if (error) throw error;

      response.send(result);
    });
  });

  // Display a single user by ID
  app.get("/site/:id", (request, response) => {
    const id = request.params.id;

    pool.query("SELECT * FROM sitio WHERE id = ?", id, (error, result) => {
      if (error) throw error;

      response.send(result);
    });
  });

  // Muestra sitios por categorias
  app.get("/categories/sites/:id", (request, response) => {
    const id = request.params.id;

    pool.query("select * from sitio left join categoria on sitio.cat_id = categoria.id where categoria.id = ?", id, (error, result) => {
      if (error) throw error;

      response.send(result);
    });
  });
};

// Export the router
module.exports = router;



// curl -d "name=Portal 80&cat_id=3" http://localhost:3002/sites