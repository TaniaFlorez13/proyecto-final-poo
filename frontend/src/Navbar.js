import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';

const navStyle = {
    color: 'white',
    textDecoration: 'none'
}

function Navbar() {
  return (
    <nav className="">
        <h3>Logo</h3>
        <ul className="nav-links">
            <Link style={navStyle}  to="/life">
                <li>Vida de la localidad</li>
            </Link>
            <Link style={navStyle} to="/interests">
                <li>Sitios de interés</li>
            </Link>
        </ul>
    </nav>
  );
}

export default Navbar;
