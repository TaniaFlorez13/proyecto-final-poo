import React, { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios'
import InterestMap from './InterestMap'

function Life() {

  const [items, setItems] = useState([])

  useEffect(() => {
    axios.get('http://localhost:3002/sites')
      .then(res => {
        setItems(res.data)
      })
      .catch(err => {
        console.log(err)
      })
  });


  return (

    <div>
      <InterestMap></InterestMap>
      {items.map(item => (
        <h1 key={item.id}>{item.name}</h1>
      ))}
    </div>
  );
}

export default Life;
