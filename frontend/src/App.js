import React from 'react';
import './App.css';
import Navbar from './Navbar';
import Interests from './Interests'
import Life from './Life'
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
      <Navbar></Navbar>
      <Switch>
        <Route path="/" exact component={Home}></Route>
        <Route path="/life" component={Life}  ></Route>
        <Route path="/interests" component={Interests}  ></Route>
      </Switch>


      </div>
    </Router>
  );
}


const Home = () => (
  <div>
    <h1>Home Page</h1>
  </div>
);

export default App;
