
import React from 'react';
import './App.css';
import L from 'leaflet';
import { Map, Marker, Popup, TileLayer, Polygon, Tooltip } from "react-leaflet";

function InterestMap() {

  let parkData = {
    "features": [
      {
        "type": "Feature",
        "properties": {
          "NAME": "parkData ",
          "DESCRIPTIO": "Flat asphalt surface, 5 components"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [4.716933097646829,-74.09994542598724]
        }
      },
      
      {
        "type": "Feature",
        "properties": {
          "PARK_ID": 1219,
          "NAME": "Bob MacQuarrie Skateboard Park (SK8 Extreme Park)",
          "DESCRIPTIO": "Flat asphalt surface, 10 components, City run learn to skateboard programs, City run skateboard camps in summer"
        },
        "geometry": {
          "type": "Point",
          "coordinates": [ 4.71291805272128, -74.11076009273529]
        }
        
      }
    ]
  } 

  const polygon = [
    [4.65346488328521, -74.10338401794434],
    [4.684475260534678, -74.12467002868652],
    [4.688517201692816, -74.11831855773926],
    [4.692003089837433, -74.12050724029541],
    [4.721942515285971, -74.16020393371582],
    [4.740157, -74.126206],
    [4.726076, -74.104145],
    [4.711060, -74.090951],
    [4.685773, -74.076949]
]




  return (
    <Map center={[4.701327220394075 ,-74.11359786987305]} zoom={12}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      />
      {parkData.features.map(park => (
     <Marker position={[ 4.66498165575986, -74.09767627716064]}>
       <Popup>Popup for Marker</Popup>
        <Tooltip>{ park.properties.NAME }</Tooltip>
    </Marker>
        ))}
        <Polygon color="purple" positions={polygon} />
    </Map>
  );
}

export default InterestMap;
